package lvl0fixpipeline.labyrinth;

import lvl0fixpipeline.global.LwjglWindow;
import lvl0fixpipeline.labyrinth.render.Renderer;

import java.awt.*;

public class App {

	public static void main(String[] args) {
		new LwjglWindow(new Renderer());
	}
}
