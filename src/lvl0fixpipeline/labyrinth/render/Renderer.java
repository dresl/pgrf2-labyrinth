package lvl0fixpipeline.labyrinth.render;

import javafx.util.Pair;
import lvl0fixpipeline.global.AbstractRenderer;
import lvl0fixpipeline.global.GLCamera;
import lvl0fixpipeline.labyrinth.maze.MazeGenerator;
import lvl0fixpipeline.labyrinth.gui.Menu;

import org.lwjgl.BufferUtils;
import org.lwjgl.glfw.*;
import transforms.Point2D;
import transforms.Vec3D;
import java.io.IOException;
import java.nio.DoubleBuffer;

import static lvl0fixpipeline.global.GluUtils.gluPerspective;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL33.*;

/**
 * Shows rendering of labyrinth
 *
 * @author resldo1
 */
public class Renderer extends AbstractRenderer {
    private float dx, dy, ox, oy;
    private float zenith, azimuth;

    private final Menu menu = new Menu(width, height);
    private final StateController state = new StateController();
    private final MazeGenerator mazeGenerator = new MazeGenerator();
    private MOVE_DIRECTIONS moveDirection = MOVE_DIRECTIONS.DOWN;
    private Scene scene;

    public Renderer() {
        super();

        glfwKeyCallback = new GLFWKeyCallback() {
            @Override
            public void invoke(long window, int key, int scancode, int action, int mods) {
                if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE && state.getLastPosition() != null) {
                    switch (state.getGameState()) {
                        case GAME:
                            state.setLastPosition(scene.getCamera().getPosition());
                            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
                            state.setGameState(GameState.MENU);
                            break;
                        case MENU:
                            scene.getCamera().setZenith(Math.toRadians(zenith));
                            scene.getCamera().setAzimuth(Math.toRadians(azimuth));
                            scene.getCamera().setPosition(state.getLastPosition());
                            // fix back to proper azimuth and zenith after mouse move
                            glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
                            DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                            DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                            glfwGetCursorPos(window, xBuffer, yBuffer);
                            ox = (float)xBuffer.get(0);
                            oy = (float)yBuffer.get(0);
                            state.setGameState(GameState.GAME);
                            break;
                    }
                }

                // disable keys in menu
                if (state.getGameState() == GameState.MENU && action != GLFW_RELEASE) {
                    switch (key) {
                        case GLFW_KEY_I:
                            state.incrementMazeWidth();
                            break;
                        case GLFW_KEY_J:
                            state.decrementMazeWidth();
                            break;
                        case GLFW_KEY_O:
                            state.incrementMazeHeight();
                            break;
                        case GLFW_KEY_K:
                            state.decrementMazeHeight();
                            break;
                    }
                    return;
                }

                if (action == GLFW_PRESS) {
                    switch (key) {
                        case GLFW_KEY_F:
                            state.toggleFlying();
                            break;
                        case GLFW_KEY_L:
                            state.toggleFlashlight();
                            break;
                    }
                }
                if (action != GLFW_RELEASE && !state.isWin()) {
                    switch (key) {
                        case GLFW_KEY_W:
                            if (state.isFlying()) {
                                scene.getCamera().forward(StateController.TRANS);
                            } else {
                                moveWithoutZenith(MOVE_DIRECTIONS.FORWARD);
                            }
                            break;
                        case GLFW_KEY_S:
                            if (state.isFlying()) {
                                scene.getCamera().backward(StateController.TRANS);
                            } else {
                                moveWithoutZenith(MOVE_DIRECTIONS.BACKWARD);
                            }
                            break;

                        case GLFW_KEY_A:
                            if (state.isFlying()) {
                                scene.getCamera().left(StateController.TRANS);
                            } else {
                                moveWithoutZenith(MOVE_DIRECTIONS.LEFT);
                            }
                            break;
                        case GLFW_KEY_D:
                            if (state.isFlying()) {
                                scene.getCamera().right(StateController.TRANS);
                            } else {
                                moveWithoutZenith(MOVE_DIRECTIONS.RIGHT);
                            }
                            break;
                        case GLFW_KEY_UP:
                            if (state.isFlying()) {
                                scene.getCamera().move(new Vec3D(0, StateController.TRANS, 0));
                            }
                            break;
                        case GLFW_KEY_DOWN:
                            if (state.isFlying()) {
                                scene.getCamera().move(new Vec3D(0, -StateController.TRANS, 0));
                            }
                            break;
                    }
                }
            }
        };

        glfwMouseButtonCallback = new GLFWMouseButtonCallback() {

            @Override
            public void invoke(long window, int button, int action, int mods) {
                DoubleBuffer xBuffer = BufferUtils.createDoubleBuffer(1);
                DoubleBuffer yBuffer = BufferUtils.createDoubleBuffer(1);
                glfwGetCursorPos(window, xBuffer, yBuffer);
                double x = xBuffer.get(0);
                double y = yBuffer.get(0);

                state.setMouseButton1(glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1) == GLFW_PRESS);

                if (button == GLFW_MOUSE_BUTTON_1 && action == GLFW_PRESS) {
                    ox = (float) x;
                    oy = (float) y;
                    if (state.getGameState() == GameState.MENU) {
                        switch (menu.btnClicked(x, y)) {
                            case 0:
                                state.setGameState(GameState.GAME);
                                scene.setMaze(mazeGenerator.generate(state.getMazeWidth(), state.getMazeHeight()));
                                mazeGenerator.display();
                                zenith = 0;
                                azimuth = 180;
                                scene.getCamera().setPosition(new Vec3D(
                                        8 + 16*scene.getMaze().getStartPos().getY(),
                                        8.2f,
                                        -10 + 16*scene.getMaze().getStartPos().getX()
                                ));
                                state.setLastPosition(scene.getCamera().getPosition());
                                scene.getCamera().setAzimuth(Math.toRadians(azimuth));
                                scene.getCamera().setZenith(Math.toRadians(zenith));
                                state.clearCollectedObjects();
                                state.setWin(false);
                                glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
                                break;
                            case 1:
                                glfwSetWindowShouldClose(window, true);
                                break;
                        }
                    }
                }
            }
        };

        glfwCursorPosCallback = new GLFWCursorPosCallback() {
            @Override
            public void invoke(long window, double x, double y) {
                state.setCurrentMousePosition(new Point2D(x, y));
                if (state.getGameState() == GameState.GAME) {
                    dx = (float) x - ox;
                    dy = (float) y - oy;
                    ox = (float) x;
                    oy = (float) y;
                    zenith -= dy / width * 180;
                    if (zenith > 90)
                        zenith = 90;
                    if (zenith <= -90)
                        zenith = -90;
                    azimuth += dx / height * 180;
                    azimuth = azimuth % 360;
                    scene.getCamera().setAzimuth(Math.toRadians(azimuth));
                    scene.getCamera().setZenith(Math.toRadians(zenith));
                    dx = 0;
                    dy = 0;
                }
            }
        };

        glfwWindowSizeCallback = new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long window, int w, int h) {
                if (w > 0 && h > 0) {
                    width = w;
                    height = h;
                    System.out.println("Windows resize to [" + w + ", " + h + "] + menu");
                    if (textRenderer != null) {
                        textRenderer.resize(width, height);
                    }
                    menu.setWidth(width);
                    menu.setHeight(height);
                }
            }
        };
    }

    private void moveWithoutZenith(MOVE_DIRECTIONS dir) {
        zenith = (float)Math.toDegrees(scene.getCamera().getZenith());
        scene.getCamera().setZenith(0);
        switch (dir) {
            case FORWARD:
                scene.getCamera().forward(StateController.TRANS);
                if (scene.getMaze().isCollisionWithWall(scene.getCamera().getPosition())) {
                    scene.getCamera().backward(StateController.TRANS);
                }
                if (scene.getMaze().isCollisionWithObject(scene.getCamera().getPosition())) {
                    state.incrementCollectedObjects();
                }
                if (this.win()) {
                    state.setWin(true);
                }
                break;
            case BACKWARD:
                scene.getCamera().backward(StateController.TRANS);
                if (scene.getMaze().isCollisionWithWall(scene.getCamera().getPosition())) {
                    scene.getCamera().forward(StateController.TRANS);
                }
                break;
            case LEFT:
                scene.getCamera().left(StateController.TRANS);
                if (scene.getMaze().isCollisionWithWall(scene.getCamera().getPosition())) {
                    scene.getCamera().right(StateController.TRANS);
                }
                break;
            case RIGHT:
                scene.getCamera().right(StateController.TRANS);
                if (scene.getMaze().isCollisionWithWall(scene.getCamera().getPosition())) {
                    scene.getCamera().left(StateController.TRANS);
                }
                break;
        }
        switch(moveDirection) {
            case DOWN:
                state.moveAvatarDown();
                scene.getCamera().setPosition(scene.getCamera().getPosition().withY(8.2f + state.getAvatarHeight()));
                if (state.getAvatarHeight() <= 0f) {
                    moveDirection = MOVE_DIRECTIONS.UP;
                }
                break;
            case UP:
                state.moveAvatarUp();
                scene.getCamera().setPosition(scene.getCamera().getPosition().withY(8.2f + state.getAvatarHeight()));
                if (state.getAvatarHeight() >= 1f) {
                    moveDirection = MOVE_DIRECTIONS.DOWN;
                }
                break;
        }
        scene.getCamera().setZenith(Math.toRadians(zenith));
    }

    /**
     * if current position is in red area
     */
    private boolean win() {
        return scene.getMaze().isInEndArea(scene.getCamera().getPosition()) &&
                state.getCollectedObjects() == scene.getMaze().getObjectsCount();
    }

    @Override
    public void init() {
        super.init();
        glClearColor(0.1f, 0.1f, 0.1f, 1.0f);

        glEnable(GL_DEPTH_TEST);
        glDisable(GL_CULL_FACE);
        glFrontFace(GL_CW);
        glPolygonMode(GL_FRONT, GL_FILL);
        glPolygonMode(GL_BACK, GL_FILL);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        System.out.println("Loading textures...");
        try {
            menu.loadTextures();
        } catch (IOException e) {
            e.printStackTrace();
        }

        scene = new Scene();
    }

    @Override
    public void display() {
        glViewport(0, 0, width, height);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_TEXTURE_2D);

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();

        gluPerspective(45, width / (float) height, 0.1f, 500.0f);

        if (state.getGameState() == GameState.MENU) {
            drawMenu();
            this.drawAppInfo();
            this.drawMenuControls();
            return;
        }

        state.setAngle((state.getAngle() + 0.07f) % 360);

        GLCamera cameraSky = new GLCamera(scene.getCamera());
        cameraSky.setPosition(new Vec3D());

        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();

        glPushMatrix();
            cameraSky.setMatrix();
            glRotatef(200+state.getAngle(), 1, 0.2f, 0);
            scene.renderSkyBox();
        glPopMatrix();

        scene.getCamera().setMatrix();  // set view transformation

        glEnable(GL_FOG);
        glFogi(GL_FOG_MODE, GL_LINEAR);
        glFogi(GL_FOG_START, 0);
        if (state.getFogThunder() < 40) {
            state.setFogThunder(state.getFogThunder() + 0.3f);
            glFogi(GL_FOG_END, (int)state.getFogThunder() + 40);
            glFogf(GL_FOG_DENSITY, 0.05f);
            glFogfv(GL_FOG_COLOR, new float[]{0.1f, 0.1f, 0.1f, 0.5f});
        } else {
            glDisable(GL_FOG);
            state.setFogThunder(-400);
        }

        // render main scene objects
        scene.render(state);

        glDisable(GL_TEXTURE_2D);
        glDisable(GL_LIGHTING);
        glDisable(GL_LIGHT0);
        glDisable(GL_LIGHT1);
        glDisable(GL_LIGHT2);
        glDisable(GL_LIGHT3);
        glDisable(GL_LIGHT4);
        glDisable(GL_LIGHT5);

        this.drawAppControls();
        this.drawAppInfo();
    }

    private void drawAppControls () {
        String text = "Ovládání: Esc (menu), ";
        if (state.isFlashlight())
            text += "[L]ight";
        else
            text += "[l]ight";

        if (state.isFlying())
            text += ", [F]lying";
        else
            text += ", [f]lying";

        text += String.format(",   Collected objects: %d/%d", state.getCollectedObjects(), scene.getMaze().getObjectsCount());

        if (state.isWin()) {
            text += ",   YOU WON!";
        }

        String textInfo = "position " + scene.getCamera().getPosition().toString();
        textInfo += String.format(" azimuth %3.1f, zenith %3.1f", azimuth, zenith);

        //create and draw text
        textRenderer.clear();
        textRenderer.addStr2D(3, 20, text);
        textRenderer.addStr2D(3, 40, textInfo);
    }

    private void drawMenuControls() {
        String text = "Nápověda: j/i zvětšení/zmenšení šířky, k/o zvětšení/zmenšení výšky";
        textRenderer.addStr2D(3, 20, text);
    }

    private void drawAppInfo() {
        textRenderer.addStr2D(width - 560, height - 3, " (c) 2022 PGRF2 UHK - Semestrální projekt - bludiště (resldo1 - Dominik Resl, 22.4. 2022)");
    }

    private void drawMenu() {
        scene.getCamera().setMatrix();
        scene.getCamera().setPosition(new Vec3D(10,7,180));
        scene.getCamera().setZenith(0);
        scene.getCamera().setAzimuth(0);
        menu.render(state.getCurrentMousePosition().getX(), state.getCurrentMousePosition().getY());

        textRenderer.addStr2D(
                (int)(0.59*width), (int)(0.63*height),
                String.format("Rozměry bludiště: %d x %d", state.getMazeWidth(), state.getMazeHeight())
        );
    }
}
