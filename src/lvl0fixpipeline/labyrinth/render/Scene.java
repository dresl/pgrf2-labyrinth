package lvl0fixpipeline.labyrinth.render;

import lvl0fixpipeline.global.GLCamera;
import lvl0fixpipeline.labyrinth.maze.Maze;
import lvl0fixpipeline.labyrinth.objects.Floor;
import lvl0fixpipeline.labyrinth.objects.Wall;
import lvl0fixpipeline.labyrinth.objects.WaterBubble;
import lwjglutils.OGLTexture2D;
import transforms.Vec3D;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static lvl0fixpipeline.global.GlutUtils.*;
import static org.lwjgl.opengl.GL11.*;

/**
 * Represents labyrinth's scene
 *
 * @author resldo1
 */
public class Scene {
    private GLCamera camera;
    private OGLTexture2D[] textureCube;
    private OGLTexture2D wallTexture, grassTexture, grassRedTexture, grassBlueTexture, waterTexture;
    private Maze maze;

    static class Light {
        int lightID;
        boolean enable;
        float r,g,b;

        public Light(int lightID, boolean enable, float r, float g, float b) {
            this.lightID = lightID;
            this.enable = enable;
            this.r = r;
            this.g = g;
            this.b = b;
        }
    }

    private final List<Light> lights = new ArrayList<>();

    public Scene() {
        camera = new GLCamera();
        camera.setPosition(new Vec3D(0,0,0));
        camera.setFirstPerson(true);

        // skybox prepare
        textureCube = new OGLTexture2D[6];
        System.out.println("Loading textures...");
        try {
            textureCube[0] = new OGLTexture2D("textures/sky-positive-x.png");
            textureCube[1] = new OGLTexture2D("textures/sky-negative-x.png");
            textureCube[2] = new OGLTexture2D("textures/sky-positive-y.png");
            textureCube[3] = new OGLTexture2D("textures/sky-negative-y.png");
            textureCube[4] = new OGLTexture2D("textures/sky-positive-z.png");
            textureCube[5] = new OGLTexture2D("textures/sky-negative-z.png");
            wallTexture = new OGLTexture2D("textures/wall-brick.jpg");
            grassTexture = new OGLTexture2D("textures/grass.jpg");
            grassRedTexture = new OGLTexture2D("textures/grass-red.jpg");
            grassBlueTexture = new OGLTexture2D("textures/grass-blue.jpg");
            waterTexture = new OGLTexture2D("textures/water.jpg");
        } catch (IOException e) {
            e.printStackTrace();
        }
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        // lights
        float[] mat_dif = new float[]{1, 1, 1, 1};
        float[] mat_spec = new float[]{1, 1, 1, 1};
        float[] mat_amb = new float[]{.1f, .1f, .1f, 1};

        glMaterialfv(GL_FRONT_AND_BACK, GL_AMBIENT, mat_amb);
        glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, mat_dif);
        glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, mat_spec);
        glMaterialfv(GL_FRONT_AND_BACK, GL_EMISSION, new float[]{0, 0, 0, 1});

        // moonlight
        setLight(GL_LIGHT0, 0.8f, 0.8f, 0.8f);
        setLight(GL_LIGHT1, 0.1f, 0.1f, 0.2f);
        setFlashLight();

        // disco lights in the end if you win
        setLight(GL_LIGHT3, 0.5f, 0f, 0f);
        setLight(GL_LIGHT4, 0f, 0.5f, 0f);
        setLight(GL_LIGHT5, 0.5f, 0.5f, 0.5f);
    }

    public GLCamera getCamera() {
        return camera;
    }

    private void renderLights(StateController state) {
        // primary moving light in model space (moon)
        glPushMatrix();
            glRotatef(1760+state.getAngle(), 1, 0.2f, 0);
            glTranslatef(0, 600, 0);
            enableLight(1);
        glPopMatrix();
        // secondary static light in model space
        glPushMatrix();
            glTranslatef(-300, 80, 240);
            enableLight(2);
        glPopMatrix();
        // disco lights
        if (state.isWin()) {
            glPushMatrix();
                glRotatef(state.getAngle()*65, 1, 0.2f, 0);
                glTranslatef(0, 150, 0);
                enableLight(3);
            glPopMatrix();
            glPushMatrix();
                glRotatef(state.getAngle()*65, 0.2f, 1f, 0);
                glTranslatef(0, -150, 0);
                enableLight(4);
            glPopMatrix();
            glPushMatrix();
                glRotatef(state.getAngle()*65, 0f, 1f, 0.5f);
                glTranslatef(150, 150, 150);
                enableLight(5);
            glPopMatrix();
        }
    }

    private void setLight(int light, float r, float g, float b) {
        float[] light_amb = new float[]{0, 0, 0, 1};//{ 0.0f*r, .1f*g, .1f*b, 1 };
        float[] light_dif = new float[]{r, g, b, 1};//{ 0.1f*r, .2f*g, .2f*b, 1 };
        float[] light_spec = new float[]{0, 0, 0, 1};//{ 0.1f*r, .3f*g, .3f*b, 1 };

        glLightfv(light, GL_AMBIENT, light_amb);
        glLightfv(light, GL_DIFFUSE, light_dif);
        glLightfv(light, GL_SPECULAR, light_spec);

        lights.add(new Light(light, true, r,g,b));
    }

    private void drawBall(float r, float g, float b, float x, float y, float z) {
        glPushMatrix();
        glTranslatef(x, y, z);
        glDisable(GL_TEXTURE_2D);
        glDisable(GL_LIGHTING);
        glColor3f(r, g, b);
        glutSolidSphere(0.5, 8, 8);
        glPopMatrix();
    }


    private void enableLight(int lightIdx) {
        float[] lightPosition = {0, 0, 0, 1};
        Light light =  lights.get(lightIdx-1);
        if (light.enable) {
            glEnable(light.lightID);
            glLightfv(light.lightID, GL_POSITION, lightPosition);
            glPushMatrix();
                drawBall(light.r, light.g, light.b, 0, 0, 0 );
            glPopMatrix();
        }
    }

    private void setFlashLight() {
        // light source setting - specular component
        float[] light_spec = new float[]{0.3f, 0.3f, 0.3f, 1};
        // light source setting - diffuse component
        float[] light_dif = new float[]{0.4f, 0.4f, 0f, 1};
        // light source setting - ambient component
        float[] light_amb = new float[]{0.3f, 0.3f, 0.3f, 1};

        glLightfv(GL_LIGHT2, GL_AMBIENT, light_amb);
        glLightfv(GL_LIGHT2, GL_DIFFUSE, light_dif);
        glLightfv(GL_LIGHT2, GL_SPECULAR, light_spec);
        glLightf(GL_LIGHT2, GL_SPOT_EXPONENT, 50);
    }

    private void renderFlashlight() {
        float[] light_position;
        light_position = new float[]{
                (float) camera.getPosition().getX()-(float)(15*camera.getEyeVector().getX()),
                (float) camera.getPosition().getY()-(float)(15*camera.getEyeVector().getY()),
                (float) camera.getPosition().getZ()-(float)(15*camera.getEyeVector().getZ()),
                1.0f
        };
        glLightfv(GL_LIGHT2, GL_POSITION, light_position);
        float[] light_direction = {
                (float) camera.getEyeVector().getX(),
                (float) camera.getEyeVector().getY(),
                (float) camera.getEyeVector().getZ(), 0f
        };
        glEnable(GL_LIGHT2);
        glLightf(GL_LIGHT2, GL_SPOT_CUTOFF, 80);
        glLightfv(GL_LIGHT2, GL_SPOT_DIRECTION, light_direction);
    }

    public void renderSkyBox() {
        glPushMatrix();
        glColor3d(0.5, 0.5, 0.5);
        int size = 250;
        //glutWireCube(size); //neni nutne, pouze pro znazorneni tvaru skyboxu

        glEnable(GL_TEXTURE_2D);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        textureCube[1].bind(); //-x  (left)
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(-size, -size, -size);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(-size, size, -size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(-size, size, size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(-size, -size, size);
        glEnd();

        textureCube[0].bind();//+x  (right)
        glBegin(GL_QUADS);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(size, -size, -size);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(size, -size, size);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(size, size, size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(size, size, -size);
        glEnd();

        textureCube[3].bind(); //-y bottom
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(-size, -size, -size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(size, -size, -size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(size, -size, size);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(-size, -size, size);
        glEnd();

        textureCube[2].bind(); //+y  top
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(-size, size, -size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(size, size, -size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(size, size, size);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(-size, size, size);
        glEnd();

        textureCube[5].bind(); //-z
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(size, -size, -size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(-size, -size, -size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(-size, size, -size);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(size, size, -size);
        glEnd();

        textureCube[4].bind(); //+z
        glBegin(GL_QUADS);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3d(-size, size, size);
        glTexCoord2f(0.0f, 1.0f);
        glVertex3d(-size, -size, size);
        glTexCoord2f(1.0f, 1.0f);
        glVertex3d(size, -size, size);
        glTexCoord2f(1.0f, 0.0f);
        glVertex3d(size, size, size);
        glEnd();

        glDisable(GL_TEXTURE_2D);
        glPopMatrix();
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
    }

    private void renderMaze(StateController state) {
        // draw floor
        for (int i = 0; i < maze.getWidth(); i++) {
            glPushMatrix();
            glTranslatef(0,0,16*i);
            for (int j = 0; j < maze.getHeight(); j++) {
                glPushMatrix();
                    glTranslatef(16*j,0,0);
                    if (i == maze.getStartPos().getX() && j == maze.getStartPos().getY()) {
                        new Floor(grassBlueTexture).render();
                    } else if (i == maze.getWidth()-1 && j == maze.getHeight()-1) {
                        new Floor(grassRedTexture).render();
                    } else {
                        new Floor(grassTexture).render();
                        if (maze.hasCellObject(i, j)) {
                            glPushMatrix();
                                glTranslatef(8, 6, -12);
                                new WaterBubble(waterTexture).withState(state).render();
                            glPopMatrix();
                        }
                    }
                glPopMatrix();
            }
            glPopMatrix();
        }

        // draw the north edge
        glPushMatrix();
        for (int i = 0; i < maze.getHeight(); i++) {
            glPushMatrix();
            for (int j = 0; j < maze.getWidth(); j++) {
                if ((maze.getCell(j, i) & 1) == 0) {
                    // "+---"
                    // x = <-2; 2>, z = <-18; -2>
                    new Wall(wallTexture).render();
                    glTranslatef(0,0, 16);
                } else {
                    // "+   "
                    glTranslatef(0,0, 16);
                }
            }
            glPopMatrix();
            glTranslatef(16, 0, 0);
        }
        glPopMatrix();

        // draw the west edge
        glPushMatrix();
        glTranslatef(-4,0,0);
        for (int i = 0; i < maze.getHeight(); i++) {
            glPushMatrix();
                for (int j = 0; j < maze.getWidth(); j++) {
                    if ((maze.getCell(j, i) & 8) == 0) {
                        // "|   "
                        glPushMatrix();
                            glTranslatef(0,0,-20);
                            glRotatef(-90, 0, 1, 0);
                            new Wall(wallTexture, true).render();
                        glPopMatrix();
                        glTranslatef(0,0, 16);
                    } else {
                        glTranslatef(0,0, 16);
                    }
                }
                glPushMatrix();
                    glTranslatef(0,0,-20);
                    glRotatef(-90, 0, 1, 0);
                    new Wall(wallTexture, true).render();
                glPopMatrix();
            glPopMatrix();
            glTranslatef(16, 0, 0);
        }
        glPopMatrix();

        // draw the bottom line
        glPushMatrix();
        for (int i = 0; i < maze.getHeight(); i++) {
            glTranslatef(16, 0, 0);
        }
        for (int j = 0; j < maze.getWidth(); j++) {
            new Wall(wallTexture).render();
            glTranslatef(0,0, 16);
        }
        glPopMatrix();
    }

    public void render(StateController state) {
        // flashlight moving with camera
        if (state.isFlashlight()) {
            renderFlashlight();
        }

        // lights in scene
        renderLights(state);

        // render main maze
        renderMaze(state);
    }

    public Maze getMaze() {
        return maze;
    }

    public void setMaze(Maze maze) {
        this.maze = maze;
    }
}
