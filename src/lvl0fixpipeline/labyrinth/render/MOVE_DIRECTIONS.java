package lvl0fixpipeline.labyrinth.render;

public enum MOVE_DIRECTIONS {
    FORWARD, BACKWARD, LEFT, RIGHT, DOWN, UP
}
