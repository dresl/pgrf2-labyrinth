package lvl0fixpipeline.labyrinth.render;


import transforms.Point2D;
import transforms.Vec3D;

public class StateController {

    GameState gameState = GameState.MENU;
    public static final float TRANS = 0.55f;

    private Point2D currentMousePosition = new Point2D(0,0);
    private Vec3D lastPosition;
    private boolean mouseButton1 = false;
    private boolean flying = false;
    private boolean flashlight = false;
    private boolean win = false;
    private int mazeWidth = 8, mazeHeight = 5;
    private float angle = 0;
    private float fogThunder = -200;
    private int collectedObjects = 0;
    // avatar moves
    private static final float AVATAR_MOVE_Y_OFFSET = 0.05f;
    private float avatarHeight = 0f;

    public StateController() {
    }

    public GameState getGameState() {
        return gameState;
    }

    public void setGameState(GameState gameState) {
        this.gameState = gameState;
    }

    public Point2D getCurrentMousePosition() {
        return currentMousePosition;
    }

    public void setCurrentMousePosition(Point2D currentMousePosition) {
        this.currentMousePosition = currentMousePosition;
    }

    public Vec3D getLastPosition() {
        return lastPosition;
    }

    public void setLastPosition(Vec3D lastPosition) {
        this.lastPosition = lastPosition;
    }

    public boolean isMouseButton1() {
        return mouseButton1;
    }

    public void setMouseButton1(boolean mouseButton1) {
        this.mouseButton1 = mouseButton1;
    }

    public boolean isFlying() {
        return flying;
    }

    public void toggleFlying() {
        flying = !flying;
    }

    public boolean isFlashlight() {
        return flashlight;
    }

    public void toggleFlashlight() {
        flashlight = !flashlight;
    }

    public int getMazeWidth() {
        return mazeWidth;
    }

    public void incrementMazeWidth() {
        if (mazeWidth < 40) {
            this.mazeWidth++;
        }
    }

    public void decrementMazeWidth() {
        if (mazeWidth > 3) {
            this.mazeWidth--;
        }
    }

    public int getMazeHeight() {
        return mazeHeight;
    }

    public void incrementMazeHeight() {
        if (mazeHeight < 40) {
            this.mazeHeight++;
        }
    }

    public void decrementMazeHeight() {
        if (mazeHeight > 3) {
            this.mazeHeight--;
        }
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public float getFogThunder() {
        return fogThunder;
    }

    public void setFogThunder(float fogThunder) {
        this.fogThunder = fogThunder;
    }

    public float getAvatarHeight() {
        return avatarHeight;
    }

    public void moveAvatarDown() {
        avatarHeight -= AVATAR_MOVE_Y_OFFSET;
    }

    public void moveAvatarUp() {
        avatarHeight += AVATAR_MOVE_Y_OFFSET;
    }

    public int getCollectedObjects() {
        return collectedObjects;
    }

    public void incrementCollectedObjects() {
        collectedObjects++;
    }

    public void clearCollectedObjects() {
        collectedObjects = 0;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }
}
