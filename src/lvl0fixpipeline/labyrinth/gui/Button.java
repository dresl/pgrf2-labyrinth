package lvl0fixpipeline.labyrinth.gui;

import lwjglutils.OGLTexture2D;

import java.awt.*;

import static org.lwjgl.opengl.GL33.*;

public class Button {
    int offsetX, offsetY;
    float xRelativeView, yRelativeView;
    OGLTexture2D texture, textureHover;

    public Button(int x, int y, OGLTexture2D texture, OGLTexture2D textureHover, float xRelativeView, float yRelativeView) {
        offsetX = x;
        offsetY = y;
        this.texture = texture;
        this.textureHover = textureHover;
        this.xRelativeView = xRelativeView;
        this.yRelativeView = yRelativeView;
    }

    public void draw(double x, double y, int width, int height) {

        if(isMouseOver(x, y, width, height)) {
            textureHover.bind();
        } else {
            texture.bind();
        }
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_REPLACE);

        glMatrixMode(GL_TEXTURE);
        glRotatef(180,0, 1, 0);
        glBegin(GL_QUADS);
        {
            glTexCoord2f(0,0);
            glVertex2d(30-offsetX, 30-offsetY);
            glTexCoord2f(-1,0);
            glVertex2d(90-offsetX, 30-offsetY);
            glTexCoord2f(-1,-1);
            glVertex2d(90-offsetX, 50-offsetY);
            glTexCoord2f(0,-1);
            glVertex2d(30-offsetX, 50-offsetY);
        }
        glEnd();
        glRotatef(-180,0, 1, 0);
        glMatrixMode(GL_MODELVIEW);
        glLoadIdentity();
    }

    public boolean isClicked(double x, double y, int width, int height) {
        return x/width >= xRelativeView && x/width <= xRelativeView + 0.25 &&
                y/height >= yRelativeView && y/height <= yRelativeView + 0.13;
    }

    public boolean isMouseOver(double x, double y, int width, int height) {
        return x/width >= xRelativeView && x/width <= xRelativeView + 0.25 &&
                y/height >= yRelativeView && y/height <= yRelativeView + 0.13;
    }
}