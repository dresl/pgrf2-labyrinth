package lvl0fixpipeline.labyrinth.gui;

import lwjglutils.OGLTexture2D;

import java.io.IOException;

public class Menu {
    private int width, height;
    Button btnStart, btnExit;

    public Menu(int width, int height) {
        this.width = width;
        this.height = height;
    }

    public void loadTextures() throws IOException {
        OGLTexture2D btnStartTex = new OGLTexture2D("textures/btn-start.jpg");
        OGLTexture2D btnStartTexHover = new OGLTexture2D("textures/btn-start-hover.jpg");
        OGLTexture2D btnExitTex = new OGLTexture2D("textures/btn-exit.jpg");
        OGLTexture2D btnExitTexHover = new OGLTexture2D("textures/btn-exit-hover.jpg");
        btnStart = new Button(0,0, btnStartTex, btnStartTexHover, 0.571f, 0.21f);
        btnExit = new Button(0,25, btnExitTex, btnExitTexHover, 0.571f, 0.38f);
    }

    public void render(double ox, double oy) {
        btnStart.draw(ox, oy, width, height);
        btnExit.draw(ox, oy, width, height);
    }

    public int btnClicked(double x, double y) {
        if (btnStart.isClicked(x, y, width, height)) return 0;
        if (btnExit.isClicked(x, y, width, height)) return 1;
        return -1;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
