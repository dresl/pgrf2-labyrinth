package lvl0fixpipeline.labyrinth.maze;

import transforms.Point2D;
import transforms.Vec3D;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class Maze {
    private final int width;
    private final int height;
    private final int[][][] grid;
    private final List<Rectangle> walls = new ArrayList<>();
    private final List<Rectangle> objects = new ArrayList<>();
    private final Rectangle endArea;
    private int objectsCount = 0;
    private Point2D startPos;

    public Maze(int width, int height) {
        this.width = width;
        this.height = height;
        this.grid = new int[width][height][2];
        this.endArea = new Rectangle(2+(height-1)*16, -18+(width-1)*16, 16, 16);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCell(int x, int y) {
        return grid[x][y][0];
    }

    public void setCell(int x, int y, int value) {
        grid[x][y][0] = value;
    }

    public void setObjectOnCell(int x, int y, int value) {
        grid[x][y][1] = value;
    }

    public boolean hasCellObject(int x, int y) {
        return grid[x][y][1] == 1;
    }

    public Point2D getStartPos() {
        return startPos;
    }

    public void setStartPos(Point2D startPos) {
        this.startPos = startPos;
    }

    public void addWall(Rectangle rec) {
        walls.add(rec);
    }

    public List<Rectangle> getWalls() {
        return walls;
    }

    public boolean isCollisionWithWall(Vec3D pos) {
        for (Rectangle rec : walls) {
            if (rec.contains(pos.getX(), pos.getZ())) {
                return true;
            }
        }
        return false;
    }

    public boolean isCollisionWithObject(Vec3D pos) {
        for (Rectangle rec : objects) {
            if (rec.contains(pos.getX(), pos.getZ())) {
                grid[(int)((rec.getMinY()+13) / 16)][(int)((rec.getMinX()-6) / 16)][1] = 0;
                break;
            }
        }
        return objects.removeIf(rec -> rec.contains(pos.getX(), pos.getZ()));
    }

    public List<Rectangle> getObjects() {
        return objects;
    }

    public void addMazeObject(Rectangle rec) {
        this.objects.add(rec);
    }

    public int getObjectsCount() {
        return objectsCount;
    }

    public void setObjectsCount(int objectsCount) {
        this.objectsCount = objectsCount;
    }

    public boolean isInEndArea(Vec3D pos) {
        return endArea.contains(pos.getX(), pos.getZ());
    }
}
