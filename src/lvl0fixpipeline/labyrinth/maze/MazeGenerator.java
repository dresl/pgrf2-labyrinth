package lvl0fixpipeline.labyrinth.maze;

import transforms.Point2D;
import java.awt.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;


public class MazeGenerator {
    private Maze maze;

    public MazeGenerator() {
    }

    public Maze generate(int width, int height) {
        maze = new Maze(width, height);
        int randomX = ThreadLocalRandom.current().nextInt(0, width/2 + 1);
        int randomY = ThreadLocalRandom.current().nextInt(0, height);
        maze.setStartPos(new Point2D(randomX, randomY));
        System.out.println(maze.getStartPos());
        // main generation of maze
        generateMazeFirstCell(randomX, randomY);
        // post generation
        generateCollisionRectangles();
        generateObjectsInMaze();
        maze.setObjectsCount(maze.getObjects().size());
        return maze;
    }

    public void display() {
        for (int i = 0; i < maze.getHeight(); i++) {
            // draw the north edge
            for (int j = 0; j < maze.getWidth(); j++) {
                System.out.print((maze.getCell(j, i) & 1) == 0 ? "+---" : "+   ");
            }
            System.out.println("+");
            // draw the west edge
            for (int j = 0; j < maze.getWidth(); j++) {
                System.out.print((maze.getCell(j, i) & 8) == 0 ? "|   " : "    ");
            }
            System.out.println("|");
        }
        // draw the bottom line
        for (int j = 0; j < maze.getWidth(); j++) {
            System.out.print("+---");
        }
        System.out.println("+");

        System.out.println("\nGenerated objects: " + maze.getObjects().size());
    }

    /**
     * Creates 2D rectangle for each wall
     */
    private void generateCollisionRectangles() {
        final int RECTANGLE_OFFSET = 1;
        int zOffset = -18-RECTANGLE_OFFSET;  // minimum value on Z axis
        int xOffset = -2-RECTANGLE_OFFSET;  // minimum value on X axis
        // make collision 2D rectangles (rendering row by row) - row is +Z axis
        for (int i = 0; i < maze.getHeight(); i++) {
            for (int j = 0; j < maze.getWidth(); j++) {
                if ((maze.getCell(j, i) & 1) == 0) {
                    // "+---"
                    maze.addWall(new Rectangle(xOffset, zOffset, 4+2*RECTANGLE_OFFSET, 16+2*RECTANGLE_OFFSET));
                }
                zOffset += 16;
            }
            zOffset = -18-RECTANGLE_OFFSET;
            xOffset += 16;
        }

        /* west edge */
        // init to start
        xOffset = -2-RECTANGLE_OFFSET;
        zOffset = -22-RECTANGLE_OFFSET;
        for (int i = 0; i < maze.getHeight(); i++) {
            for (int j = 0; j < maze.getWidth(); j++) {
                if ((maze.getCell(j, i) & 8) == 0) {
                    // "|   "
                    maze.addWall(new Rectangle(xOffset, zOffset, 20+2*RECTANGLE_OFFSET, 4+2*RECTANGLE_OFFSET));
                }
                zOffset += 16;
            }
            maze.addWall(new Rectangle(xOffset, zOffset, 20+2*RECTANGLE_OFFSET, 4+2*RECTANGLE_OFFSET));
            xOffset += 16;
            zOffset = -22-RECTANGLE_OFFSET;
        }

        /* bottom line */
        // init to start
        xOffset = -2-RECTANGLE_OFFSET;
        zOffset = -18-RECTANGLE_OFFSET;
        for (int i = 0; i < maze.getHeight(); i++) {
            xOffset += 16;
        }
        for (int j = 0; j < maze.getWidth(); j++) {
            maze.addWall(new Rectangle(xOffset, zOffset, 4+2*RECTANGLE_OFFSET, 16+2*RECTANGLE_OFFSET));
            zOffset += 16;
        }
    }

    private void generateMaze(int cx, int cy) {
        DIR[] dirs = DIR.values();
        Collections.shuffle(Arrays.asList(dirs));
        for (DIR dir : dirs) {
            int nx = cx + dir.dx;
            int ny = cy + dir.dy;
            if (between(nx, maze.getWidth()) && between(ny, maze.getHeight())
                    && (maze.getCell(nx, ny) == 0)) {
                maze.setCell(cx, cy, maze.getCell(cx, cy) | dir.bit);
                maze.setCell(nx, ny, maze.getCell(nx, ny) | dir.opposite.bit);
                generateMaze(nx, ny);
            }
        }
    }

    private void generateMazeFirstCell(int cx, int cy) {
        int nx = cx + DIR.E.dx;
        int ny = cy + DIR.E.dy;
        maze.setCell(cx, cy, maze.getCell(cx, cy) | DIR.E.bit);
        maze.setCell(nx, ny, maze.getCell(nx, ny) | DIR.E.opposite.bit);
        generateMaze(nx, ny);
    }

    private void generateObjectsInMaze() {
        int xOffset = 6;
        int zOffset = -13;
        for (int i = 0; i < maze.getHeight(); i++) {
            for (int j = 0; j < maze.getWidth(); j++) {
                int rand = ThreadLocalRandom.current().nextInt(0, 10);
                if (i != maze.getHeight()-1 && j != maze.getWidth()-1 && (rand == 0 || rand == 5)) {
                    Rectangle objectArea = new Rectangle(xOffset, zOffset, 4, 4);
                    Rectangle startPosRectangle = new Rectangle(
                            (int)(2+16*maze.getStartPos().getY()),
                            (int)(-18+maze.getStartPos().getX()),
                            16, 16
                    );
                    if (!startPosRectangle.contains(objectArea)) {
                        maze.setObjectOnCell(j, i, 1);
                        maze.addMazeObject(objectArea);
                    }
                }
                zOffset += 16;
            }
            zOffset = -13;
            xOffset += 16;
        }
    }

    private static boolean between(int v, int upper) {
        return v >= 0 && v < upper;
    }

    private enum DIR {
        N(1, 0, -1), S(2, 0, 1), E(4, 1, 0), W(8, -1, 0);
        private final int bit;
        private final int dx;
        private final int dy;
        private DIR opposite;

        // use the static initializer to resolve forward references
        static {
            N.opposite = S;
            S.opposite = N;
            E.opposite = W;
            W.opposite = E;
        }

        DIR(int bit, int dx, int dy) {
            this.bit = bit;
            this.dx = dx;
            this.dy = dy;
        }

        @Override
        public String toString() {
            return "DIR{" +
                    "bit=" + bit +
                    ", dx=" + dx +
                    ", dy=" + dy +
                    '}';
        }
    }
}