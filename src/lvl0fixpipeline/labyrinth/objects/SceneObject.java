package lvl0fixpipeline.labyrinth.objects;

import lvl0fixpipeline.labyrinth.render.StateController;
import lwjglutils.OGLTexture2D;

public abstract class SceneObject {
    protected OGLTexture2D texture;
    protected StateController state;

    public SceneObject(OGLTexture2D texture) {
        this.texture = texture;
    }

    public SceneObject withState(StateController state) {
        this.state = state;
        return this;
    }

    public abstract void render();
}
