package lvl0fixpipeline.labyrinth.objects;

import lwjglutils.OGLTexture2D;

import static lvl0fixpipeline.global.GlutUtils.glutSolidSphere;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL11.GL_MODELVIEW;

public class WaterBubble extends SceneObject {

    public WaterBubble(OGLTexture2D texture) {
        super(texture);
    }

    @Override
    public void render() {
        glEnable(GL_LIGHTING);
        glEnable(GL_TEXTURE_2D);
        texture.bind();

        glPushMatrix();
            glRotatef(state.getAngle()*40, 0.3f, 0.8f, 0.6f);
            glTranslatef(0, 1f, 0);
            glutSolidSphere(1.5f, 16, 16);
        glPopMatrix();

        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glMatrixMode(GL_MODELVIEW);
    }
}
