package lvl0fixpipeline.labyrinth.objects;

import lwjglutils.OGLTexture2D;

import static lvl0fixpipeline.global.GlutUtils.glutSolidCube;
import static org.lwjgl.opengl.GL11.*;

public class Floor extends SceneObject {

    public Floor(OGLTexture2D texture) {
        super(texture);
    }

    @Override
    public void render() {

        glEnable(GL_LIGHTING);
        glEnable(GL_TEXTURE_2D);
        texture.bind();

        glPushMatrix();
        glTranslatef(4, 0, 0);
        for (int k = 0; k < 4; k++) {
            glPushMatrix();
            glTranslatef(k*4, 0, 0);
            for (int i = 0; i < 4; i++) {
                glTranslatef(0, 0, -4);
                glutSolidCube(4);
            }
            glPopMatrix();
        }
        glPopMatrix();

        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glMatrixMode(GL_MODELVIEW);
    }
}
