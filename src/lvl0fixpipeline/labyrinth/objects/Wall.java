package lvl0fixpipeline.labyrinth.objects;

import lwjglutils.OGLTexture2D;

import static lvl0fixpipeline.global.GlutUtils.glutSolidCube;
import static org.lwjgl.opengl.GL11.*;

public class Wall extends SceneObject {
    private boolean vertical = false;

    public Wall(OGLTexture2D texture) {
        super(texture);
    }

    public Wall(OGLTexture2D texture, boolean vertical) {
        super(texture);
        this.vertical = vertical;
    }

    @Override
    public void render() {
        glEnable(GL_LIGHTING);
        glEnable(GL_TEXTURE_2D);
        texture.bind();

        int length = vertical ? 5 : 4;

        for (int k = 0; k < 3; k++) {
            glPushMatrix();
            glTranslatef(0, k*4, 0);
            for (int i = 0; i < length; i++) {
                glTranslatef(0, 0, -4);
                glPushMatrix();
                    glRotatef(-90,0,1,0);  // rotate to have bricks horizontally
                    glutSolidCube(4);
                glPopMatrix();
                glMatrixMode(GL_MODELVIEW);
            }
            glPopMatrix();
        }

        glDisable(GL_LIGHTING);
        glDisable(GL_TEXTURE_2D);
        glMatrixMode(GL_MODELVIEW);
    }
}
